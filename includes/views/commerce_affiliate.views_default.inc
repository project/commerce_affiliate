<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_affiliate_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'commerce_affiliate_commissions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_affiliate_commission';
  $view->human_name = 'Commerce Affiliate Commissions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'created' => 'created',
    'name_1' => 'name_1',
    'name' => 'name',
    'commerce_affiliate_amount' => 'commerce_affiliate_amount',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'created' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_affiliate_amount' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Affiliate Commission: Commission form */
  $handler->display->display_options['footer']['commission_form']['id'] = 'commission_form';
  $handler->display->display_options['footer']['commission_form']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['footer']['commission_form']['field'] = 'commission_form';
  $handler->display->display_options['footer']['commission_form']['label'] = 'commission_form';
  /* Relationship: Affiliate Commission: Affiliate affiliate_uid */
  $handler->display->display_options['relationships']['affiliate']['id'] = 'affiliate';
  $handler->display->display_options['relationships']['affiliate']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['relationships']['affiliate']['field'] = 'affiliate';
  $handler->display->display_options['relationships']['affiliate']['required'] = TRUE;
  /* Relationship: Affiliate Commission: Campaign campaign_id */
  $handler->display->display_options['relationships']['campaign']['id'] = 'campaign';
  $handler->display->display_options['relationships']['campaign']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['relationships']['campaign']['field'] = 'campaign';
  /* Field: Affiliate Commission: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'users';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'affiliate';
  $handler->display->display_options['fields']['name_1']['label'] = 'Affiliate';
  /* Field: Affiliate campaign: Campaign name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'affiliate_ng_campaign';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'campaign';
  $handler->display->display_options['fields']['name']['label'] = 'Campaign';
  /* Field: Affiliate Commission: Amount */
  $handler->display->display_options['fields']['commerce_affiliate_amount']['id'] = 'commerce_affiliate_amount';
  $handler->display->display_options['fields']['commerce_affiliate_amount']['table'] = 'field_data_commerce_affiliate_amount';
  $handler->display->display_options['fields']['commerce_affiliate_amount']['field'] = 'commerce_affiliate_amount';
  $handler->display->display_options['fields']['commerce_affiliate_amount']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_affiliate_amount']['settings'] = array(
    'calculation' => FALSE,
  );
  /* Field: Affiliate Commission: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Operations';
  /* Sort criterion: Affiliate Commission: Date created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Affiliate Commission: Parent entity id */
  $handler->display->display_options['arguments']['parent_entity_id']['id'] = 'parent_entity_id';
  $handler->display->display_options['arguments']['parent_entity_id']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['arguments']['parent_entity_id']['field'] = 'parent_entity_id';
  $handler->display->display_options['arguments']['parent_entity_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['parent_entity_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['parent_entity_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['parent_entity_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['parent_entity_id']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Affiliate Commission: Parent entity type */
  $handler->display->display_options['arguments']['parent_entity_type']['id'] = 'parent_entity_type';
  $handler->display->display_options['arguments']['parent_entity_type']['table'] = 'commerce_affiliate_commission';
  $handler->display->display_options['arguments']['parent_entity_type']['field'] = 'parent_entity_type';
  $handler->display->display_options['arguments']['parent_entity_type']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['parent_entity_type']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['parent_entity_type']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['parent_entity_type']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['parent_entity_type']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['parent_entity_type']['limit'] = '0';

  /* Display: Admin */
  $handler = $view->new_display('page', 'Admin', 'admin');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer commerce_affiliate_commission entities';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'affiliate';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
  );
  $handler->display->display_options['path'] = 'admin/affiliates/commissions';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Commissions';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  return $views;
}
