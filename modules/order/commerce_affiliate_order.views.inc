<?php

/**
 * @file
 * Add missing relationships to automatic Views integration provided by Entity
 * API
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_affiliate_order_views_data_alter(&$data) {
  $order_relationship = array(
    'title' => t('Affiliate Commissions'),
    'help' => t('Relate an order with the affiliate commissions attached to it.'),
    'relationship' => array(
      'label' => t('Order Affiliate Commission'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_affiliate_commission',
      'base field' => 'parent_entity_id',
      'relationship field' => 'order_id',
      'extra' => array(
        array(
          'field' => 'parent_entity_type',
          'value' => 'commerce_order',
          'operator' => '=',
        ),
      ),
    ),
  );
  $data['commerce_order']['commerce_affiliate_commission'] = $order_relationship;

  $product_relationship = array(
    'title' => t('Affiliate Commissions'),
    'help' => t('Relate a product with the affiliate commissions attached to it.'),
    'relationship' => array(
      'label' => t('Product Affiliate Commission'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_affiliate_commission',
      'base field' => 'parent_entity_id',
      'relationship field' => 'product_id',
      'extra' => array(
        array(
          'field' => 'parent_entity_type',
          'value' => 'commerce_product',
          'operator' => '=',
        ),
      ),
    ),
  );
  $data['commerce_product']['commerce_affiliate_commission'] = $product_relationship;
}
